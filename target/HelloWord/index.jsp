<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <script src="./js/res_js/jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="./layui/css/layui.css">
    <link rel="stylesheet" href="./css/adminLogin.css">
    <style>
        .loginForm{
            margin-left: 35%;
            margin-top: 10%;
            /*background-color: #cccccc;*/
            background-color: #e7e7e7;
            width: 400px;
            height: 420px;
            float: left;
            z-index: 9999;
            position: fixed;
            opacity: 0.75;
        }
    </style>
    <script type="text/javascript">
        function refresh(){
            loginForm.imgValidate.src = "validate.jsp?id=" + Math.random();
        }
    </script>
</head>
<body>
<%@include file="window.jsp"%>
<div class="wrap">
    <img src="imgs/header.jpg" class="imgStyle">
    <div class="loginForm">
        <form method="post" action="/login" name="loginForm">
            <div class="logoHead">
                <h2 style="margin-top: 15px">图书管理系统</h2>
            </div>
            <div class="usernameWrapDiv">
                <div class="usernameLabel">
                    <label>用户名:</label>
                </div>
                <div class="usernameDiv">
                    <i class="layui-icon layui-icon-username adminIcon"></i>
                    <input id="loginUsername" class="layui-input adminInput" type="text" name="username" placeholder="输入用户名" >
                </div>
            </div>
            <div class="usernameWrapDiv">
                <div class="usernameLabel">
                    <label>密码:</label>
                </div>
                <div class="passwordDiv">
                    <i class="layui-icon layui-icon-password adminIcon"></i>
                    <input id="loginPassword" class="layui-input adminInput" type="password" name="password" placeholder="输入密码">
                </div>
            </div>
            <div class="usernameWrapDiv">
                <div class="usernameLabel">
                    <label>验证码:</label>
                </div>
                <div class="cardDiv">
                    <input id="loginCard" class="layui-input cardInput" type="text" name="code" placeholder="输入验证码">
                </div>
                <div class="cardDiv">
                    <img name="imgValidate" border="0" src="validate.jsp" onclick="refresh()">
                </div>
            </div><br>
            <div style="text-align: center">
                管理员:&nbsp;<input type="radio" name="role" value="0" title="管理员">&nbsp;&nbsp;
                用户:&nbsp;<input type="radio" name="role" value="1" title="用户" checked>
            </div>
            <div class="usernameWrapDiv">
                <div class="submitLabel">
                    <label>没有账号？<a href="./register.jsp" id="loginRegister">点击注册</a></label>
                </div>
                <div class="submitDiv">
                    <input id="loginBtn" type="submit" class="submit layui-btn layui-btn-primary" value="登录"/>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="./layui/layui.js" type="text/javascript"></script>
<script>
    layui.use(['layer'],function () {
        var layer = layui.layer;
    })
</script>
</body>
</html>
