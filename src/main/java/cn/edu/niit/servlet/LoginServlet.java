package cn.edu.niit.servlet;

import cn.edu.niit.service.UserService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoginServlet",urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private UserService userService =new UserService();
    @Override
    protected void doGet(HttpServletRequest req,HttpServletResponse resp)throws IOException,ServletException{
        doPost(req,resp);//this
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        String result=userService.login(username,password,req.getSession());
        String code=req.getParameter("code");
        HttpSession session = req.getSession();
        String randStr = (String) session.getAttribute("randStr");
        if ("1".equals(result)){
            if (code.equals(randStr)){
                resp.sendRedirect("/main.jsp");
            }else {
                req.getRequestDispatcher("/index.jsp?message=" + "验证码错误").forward(req,resp);
            }
        }else {
            req.getRequestDispatcher("/index.jsp?message=" + result).forward(req,resp);
        }
    }


//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
//            throws IOException, ServletException {
//        req.setCharacterEncoding("utf-8");
//        resp.setCharacterEncoding("utf-8");
//
//        Login login=new Login(
//                req.getParameter("username"),
//                req.getParameter("password"));
//        String result= userService.login(login,req.getSession());
//        if (UserService.LOGIN_SUCCESS.equals(result)){
//            resp.sendRedirect("/main.jsp");
//        }else {
//            req.getRequestDispatcher("/index.jsp?message="+result).forward(req,resp);
//        }
//    }

}
