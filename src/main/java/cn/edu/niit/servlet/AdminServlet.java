package cn.edu.niit.servlet;

    
import cn.edu.niit.service.AdminService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet(name = "AdminServlet",urlPatterns = "/admin/login")
public class AdminServlet extends HttpServlet {

    private AdminService adminService = new AdminService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws IOException, ServletException {
        this.doPost(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException {
        String username=req.getParameter("username");
        String password=req.getParameter("password");

        String result = adminService.adminLogin(username,password,req.getSession());
        if ("1".equals(result)){
            resp.sendRedirect("/admin/main.jsp");
        }else {
            req.getRequestDispatcher("/index.jsp?message="+ URLEncoder.encode(result,"utf-8")).forward(req,resp);
        }
    }
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
//            throws IOException, ServletException {
//        req.setCharacterEncoding("utf-8");
//        resp.setCharacterEncoding("utf-8");
//
//        Login login=new Login(req.getParameter("username"),req.getParameter("password"));
//        String result=adminService.loginAdmin(login,req.getSession());
//        if (UserService.LOGIN_SUCCESS.equals(result)){
//            resp.sendRedirect("/main2.jsp");
//        }else {
//            req.getRequestDispatcher("/index.jsp?message="+result).forward(req,resp);
//        }
//    }
}
