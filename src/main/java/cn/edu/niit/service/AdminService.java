package cn.edu.niit.service;

import cn.edu.niit.dao.AdminDao;
import cn.edu.niit.javabean.Admin;
import cn.edu.niit.javabean.Login;

import javax.servlet.http.HttpSession;

public class AdminService {
    public final static String LOGIN_SUCCESS="1";
    private AdminDao adminDao=new AdminDao();

    public String adminLogin (String username,String password,HttpSession session){
        Admin admin= adminDao.selectOne(username,password);
        if (admin == null){
            return "用户不存在";
        }else {
            if (password.equals(admin.getPassword())){
                session.setAttribute("admin",admin);
                session.setAttribute("isLogin",true);
                return "1";
            }else{
                return "密码错误";
            }
        }
    }
//    public String loginAdmin(Login loginParam, HttpSession session){
//        Admin admin=adminDao.selectOne(loginParam.getUsername());
//        if (admin != null){
//            if (loginParam.getPassword().equals(admin.getPassword())){
//                session.setAttribute("admin",admin);
//                session.setAttribute("isLogin",true);
//                return LOGIN_SUCCESS;
//            }else {
//                return "密码错误";
//            }
//        }else {
//            return "用户不存在";
//        }
//    }
}
