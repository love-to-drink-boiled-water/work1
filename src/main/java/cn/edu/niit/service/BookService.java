package cn.edu.niit.service;

import cn.edu.niit.dao.BookDao;
import cn.edu.niit.javabean.Book;

import java.util.List;

public class BookService {

    private BookDao bookDao = new BookDao();

    public List<Book> searchAllBooks(int pageNum,int pageSize){
        List<Book> books = bookDao.selectAllBooks(pageNum,pageSize);
        return books;
    }
    public int countNum(){
        return bookDao.selectAllCount();
    }

    public List<Book> selectByName(String bookName,int pageNum, int pageSize){
        List<Book> book = bookDao.selectByName(bookName,pageNum,pageSize);
        return book;
    }
}
