package cn.edu.niit.dao;

import cn.edu.niit.db.JDBCUtil;
import cn.edu.niit.javabean.User;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
    //普通用户
    public User selectOne(String username){
        User user=null;
        try(ResultSet rs= JDBCUtil.getInstance().executeQueryRS(
                "select * from borrow_card where username=?",
                    new Object[]{username})){
            while (rs.next()){
                user=new User(rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("reader"),
                        rs.getString("header"),
                        rs.getString("cellphone"),
                        rs.getString("email"),
                        rs.getString("describe"),
                        rs.getBoolean("sex"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return  user;
    }

    public int addUser(User register){
        String sql=
                "INSERT IGNORE INTO borrow_card (username, `password`, reader)\n" +
                "    SELECT username, `password`, reader\n" +
                "    FROM (SELECT ? AS username, ? AS `password`, ? AS reader) t\n" +
                "    WHERE NOT EXISTS (SELECT 1 FROM borrow_card u WHERE u.username = ?)";
        return JDBCUtil.getInstance().executeUpdate(sql,
                new Object[]{register.getUsername(),
                register.getPassword(),
                register.getReader(),
                register.getUsername()});
    }

    public int updateOne(User user){
        int result = 0;
        StringBuilder sb = new StringBuilder("update borrow_card " +
                "set reader=?, cellphone=?, email=?, sex=?, " +
                "borrow_card.`describe`=? ");
        if (user.getHeader() !=null){
            sb.append(", header=? where " +
                    "username=?");
            result = JDBCUtil.getInstance().executeUpdate(sb.toString(),
                    new Object[]{user.getReader(),
                    user.getCellphone(),
                    user.getEmail(),
                    user.isSex(),
                    user.getDescribe(),
                    user.getHeader(),
                    user.getUsername()});
        }else {
            sb.append("where username=?");
            result = JDBCUtil.getInstance().executeUpdate(sb.toString(),
                    new Object[]{user.getReader(),
                    user.getCellphone(),
                    user.getEmail(),
                    user.isSex(),
                    user.getDescribe(),
                    user.getUsername()});
        }
        return result;
    }
}
