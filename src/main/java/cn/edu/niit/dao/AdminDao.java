package cn.edu.niit.dao;

import cn.edu.niit.db.JDBCUtil;
import cn.edu.niit.javabean.Admin;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AdminDao {
    //管理员
    public Admin selectOne(String username,String password){
        Admin admin=null;
        try(ResultSet rs= JDBCUtil.getInstance().executeQueryRS(
                "select * from admin where username=?",
                new Object[]{username})){
            while (rs.next()){
                admin=new Admin(rs.getString("username"),
                        rs.getString("password"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return  admin;
    }
}
