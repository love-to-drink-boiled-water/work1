package cn.edu.niit.dao;

import cn.edu.niit.db.JDBCUtil;
import cn.edu.niit.javabean.Book;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDao {
    public List<Book> selectAllBooks(int pageNum,int pageSize){
        String sql = "select books.*, book_sort.name as sort " +
                "from books, book_sort where " +
                "books.sort_id=book_sort.id limit ?,?";
        List<Book> books = new ArrayList<>();

        try (ResultSet rs =
                     JDBCUtil.getInstance().executeQueryRS(sql,
                             new Object[]{(pageNum-1) * pageSize,
                             pageSize})){
            while (rs.next()){
                Book book = new Book(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("author"),
                        rs.getString("sort"),
                        rs.getString("description"));
                books.add(book);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return books;
    }
    public int selectAllCount(){
        String sql = "select count(*) as countNum from books";
        try (ResultSet rs =
                JDBCUtil.getInstance().executeQueryRS(sql,
                        new Object[]{})){
            while (rs.next()){
                int count = rs.getInt("countNum");
                return count;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return 0;
    }
    public List<Book> selectByName(String bookName,int pageNum, int pageSize){
        String sql = "select books.*,book_sort.name as sort from books,book_sort where " +
                "books.sort_id=book_sort.id AND books.`name` LIKE CONCAT('%', ?, '%') LIMIT ?,?";
        List<Book> books = new ArrayList<>();
        try (ResultSet rs=JDBCUtil.getInstance().executeQueryRS(sql,new Object[]{bookName,(pageNum-1)*pageSize,pageSize})){
            while (rs.next()){
                Book book = new Book(rs.getInt("id"), rs.getString("name"),
                        rs.getString("author"),
                        rs.getString("sort"),
                        rs.getString("description"));
                books.add(book);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return books;
    }
}
